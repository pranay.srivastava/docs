# Build Pantavisor

For building, the flow is very similar to the Android build system, except that targets are not setup in advance of operations but rather these are conditional on the target and the flow is managed by scripts/build.sh.

## Build your chosen reference device

After you have [chosen](choose-reference-device.md) a reference device matching your device target architecture and [downloaded](get-source-code.md) the Pantavisor source code, you can now start the building process.

First of all, check you still are in your Pantavisor workspace path. Output of ```ls``` should look similar to:

```
alchemy  bootloader  build.docker.sh  build.sh  config
external  internal  kernel  out  scripts  toolchains  vendor
```

Now, you can start the compilation of all the dependencies and flashable image with your [chosen device and target](choose-reference-device.md):

```
PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/malta_initial_latest ./build.docker.sh malta-qemu
```

This will generate all dependencies of the malta-qemu build tree and build a final image according to the values defined in ```config/malta/image.config```. You can find the generated image in ```out/malta/flash-malta-16384.img```. In this case is a pflash QEMU bootable image to be used with the locally built host QEMU. To check how to run this and images for other targets, go [here](image-setup.md).

For building other locally or in-cloud stored devices, check the [PVR_MERGE_SRC build option](build-options.md#set-a-reference-device). For other advaced building options and configurations, check our [build options](build-options.md), [build components](build-components.md) or [pantavisor configuration](pantavisor-configuration.md) references.
