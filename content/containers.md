# Containers

Pantavisor implements a lightweight container run-time with the help of Linux Containers (LXC). Each container, in its minimal form, is then comprised of a rootfs that will be run isolated in its own name-space and an LXC configuration file. All these, as well as more advanced configuration, are included in the [state JSON](pantavisor-state-format-v2.md#containerrunjson).

## Storage

The most basic storage unit is always the rootfs. In addition to this, a container may define several more auxiliary [storage volumes](pantavisor-state-format-v2.md#storage). Pantavisor gives flexibility to configure persistence of changes and encryption. 

There a three types of persistence options:

* permanent: changes are stored in a single writable location.
* revision: changes are stored in a writable location pegged to the revision.
* boot: a pure tmpfs storage that will throw away changes after a reset.

Storage can be linked to a [storage disk](storage.md#disks).

## Drivers

Containers can [reference](pantavisor-state-format-v2.md#containerrunjson) the BSP [managed drivers](bsp.md#managed-drivers) as required, optional or manual.

* required: these drivers will be loaded as soon as container is [STARTED](#status). The [revision](revisions.md) will fail if the drivers are not enabled through BSP as managed drivers.
* optional: these drivers will be loaded as soon as container is [STARTED](#status) too. In this case the revision will not fail if the drivers are not defined in the BSP.
* manual: drivers can be loaded from within containers trough [local control](local-control.md). The success or failure of loading drivers using the REST API will not determine whether a revision fails or not, but the [calls](pantavisor-commands.md#drivers) will return an error response if necessary.

## Loggers

Containers, by default, will automatically direct these logs from the container to the [log server](storage.md#logs):

* syslog
* messages
* lxc log
* lxc console

This list can be expanded to other files using the [state JSON](pantavisor-state-format-v2.md#logger).

## Groups

Containers can [belong](pantavisor-state-format-v2.md#bsprunjson) to one of the default groups created by Pantavisor:

* data: for containers which volumes we want to mount but not to be started.
* root: meant for the container or containers that are in charge of setting network connectivity up for the board.
* platform: meant for middleware and utility containers. It is the default value for containers without an expecitly defined group.
* app: application level containers.

If a container does not have an explicitly defined group, it will be automatically set to _platform_, except if it is the first container in alphabetical order and no other container has been set to _root_, in which case it will be set to _root_.

Groups main function is to define the order of how containers are started: _data_ containers are mounted first, and then it follows with the sequential mount and start up of _root_, _platform_ and _app_ containers. A group will not be mounted and/or started until all [status goals](#status-goal) from all the platforms belonging to the previous group are achieved.

If not explicitely configured, group also determines the [restart policy](#restart-policy), setting to _system_ restart the containers belonging to _data_, _root_ or _platform_ groups, while the rest of containers will be set to _container_ restart.

## Roles

[Roles](pantavisor-state-format-v2.md#containerrun.json) can be set to a given container. Roles will determine the elements that Pantavisor will make available in the container rootfs. There is just one role supported for now: mgmt.

If no role is defined, Pantavisor just mounts these elements into the container under the /pantavisor path:

* [pv-ctrl socket](local-control.md) with no privileges (only allows to report [signals](#signals) to alter the [status goal](#status-goal)).
* [pv-ctrl-log socket](local-control.md) to send logs to Pantavisor.
* The stored [logs](storage.md#logs) for that container and revision.
* The stored [user metadata](metadata.md#user-metadata) and [device metadata](metadata.md#device-metadata) for that container.

In addition to this, mgmt containers get these elements in /pantavisor:

* [pv-ctrl socket](local-control.md) with privileges (full request support).
* [pv-ctrl-log socket](local-control.md) to send logs to Pantavisor.
* Full [logs](storage.md#logs) for all containers and revisions.
* The stored [user metadata](metadata.md#user-metadata) and [device metadata](metadata.md#device-metadata) for all containers.
* Challenge and device-id information for [Pantacor Hub](remote-control.md#pantacor-hub).

## Restart Policy

[Restart policy](pantavisor-state-format-v2.md#containerrunjson) defines how Pantavisor is going to [transition](updates.md#inprogress) into a new revision. There are two types of policies:

* system: any update that modifies any object or JSON belonging to at least one of the containers with _system_ restart policy will result in a [reboot transition](updates.md#reboot-transition).
* container: any update that only modifies objects or JSONs belonging to containers with the _container_ restart policy will result in a [non-reboot transition](updates.md#non-reboot-transition)

If the restart policy is not [explicitly configured](pantavisor-state-format-v2.md#containerrunjson) in a container, it will be set according to its [group](#groups).

## Status

After a new [revision](revisions.md) is [updated](updates.md), or after the board is booted up, the containers will try to start if they were not previously started (this could happen in case of a [non-reboot update](updates.md#non-reboot-transition).

These are the different status containers can be at:

* INSTALLED: the container is installed and ready to go.
* MOUNTED: the container volumes are mounted, but not yet started.
* BLOCKED: any of the [status goals](#status-goal) from a platform belonging to the previous group are not yet achieved.
* STARTING: container is starting.
* STARTED: container PID is running.
* READY: Pantavisor has received a readiness [signal](#signals) from the container.
* STOPPING: container is stopping because of a [update transition](#updates.md).
* STOPPED: container has stopped.

### Status Goal

[Status goal](pantavisor-state-format-v2.md#containerrunjson) defines the [status](#status) that Pantavisor is going to aim for a container and, ultimately, this is going to affect in how [groups](#groups) are activated.

These are the status goals currently supported:

* MOUNTED: for containers whose volumes we want to be mounted but not started.
* STARTED: rest of containers that we want mounted and started, but we only check its PID is running.
* READY: same as STARTED, but a readiness [signal](#signals) from the container namespace is required.

If the status goal is not [explicitely configured](pantavisor-state-format-v2.md#containerrunjson) in a container, it will be set according to its [group](#groups).

### Signals

Signals can be sent from the [container side](local-control.md) to Pantavisor in order to affect the container [status](#status).

For now, we only support the READY signal, which can be used if the [READY status goal](#status-goal) is set in a container.

## Additional Files

Before starting a container, Pantavisor will mount its main rootfs and any other configured [volumes](#storage). Besides this, [additional files](pantavisor-state-format-v2.md#_configcontainer) can be attached to a [revision](revisions.md) to create a new overlay that will overwrite whatever is in that location in the rootfs of the container, creating the directories or files if necessary.

Thanks to this, configuration files or scripts can be added or modified without having to do it in the rootfs itself when preparing a new revision.
