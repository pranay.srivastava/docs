# Storage

Pantavisor needs store some artifacts on disk. These are the elements on the root of the /storage space:

* boot: information written by Pantavisor so [bootloader](bsp.md#bootloader) can run the proper revision after boot up.
* cache: to store [metadata](metadata.md) and other stuff that is not fixed to [revisions](revisions.md).
* config: to store part of the [configuration](pantavisor-configuration.md#pantahubconfig).
* disks: permanent and revision [disks](#disks).
* logs: Pantavisor and container [logs](#logs).
* objects: [binary artifacts](#trails-and-objects) that are part of revisions.
* trails: list of [revisions](#trails-and-objects).

## Disks

Pantavisor offers a way to define the physical storage medium. Disks are just [storage volumes](containers.md#storage) that have _permanent_ or _revision_ persistence.

There are currently 4 disks types supported:

* Non-encrypted directory
* Device Mapper crypt using without hardware acceleration (versatile) 
* Device Mapper crypt using i.Mx CAAM
* Device Mapper crypt using i.Mx DCP

## Logs

Pantavisor can centralize both its own logs and your [container logs](containers.md#loggers), separated by [revision](revisions.md), in one place on-disk.

It does so by running a small server which offers a socket (pv-ctrl-log) where [containers](containers.md) can direct their logs into.

There is a number of parameters that can be tweaked from the [configuration](pantavisor-configuration.md#pantavisorconfig), such as log.capture, log.maxsize, log.level, etc.

## Trails and objects

Pantavisor trails, with their [state JSONs](pantavisor-state-format-v2.md) are stored for each installed revision. The rest of artifacts that can be shared between revisions, are called objects and stored separately.

## Garbage Collector

The Pantavisor garbage collector is in charge of cleaning up the /storage by automatically removing unused Pantavisor artifacts.

It works by removing [logs](#logs), [trails](#trails-and-objects) and [stored disks](#disks) that belong to old revisions. After all this, all orphan [objects](#trails-and-objects) that were linked to removed revisions are deleted too.

The garbage collector will not affect any of the files related to the running [revision](#trails-and-objects), the revision that is being updated or the latest [DONE](updates.md) revision. These revisions that are not affected by the garbage collector can be expanded to the factory revision using the [storage.gc.keep_factory configuration parameter](pantavisor-configuration.md#pantavisorconfig).

Currently, it can be triggered by three different events:

* by a [remote update](remote-control.md) that requires more disk space than available
* if a threshold of disk usage is surpassed
* with a command

### Remote update

If a [remote update](remote-control.md) requires more disk space than available, the garbage collector will be activated. This can be adjusted with the configuration [storage.gc.reserved parameter](pantavisor-configuration.md#pantavisorconfig).

**NOTE:** this type of trigger will not work with [local updates](local-control.md). For those, you will need to use one of the following two options.

### Threshold

Disk usage will be checked periodically and garbage collector will be activated if that threshold is reached. By default it is disabled. This can be changed with the [storage.gc.threshold parameter](pantavisor-configuration.md#pantavisorconfig).

If an object is put using the [objects endpoint](pantavisor-commands.md#objects), the threshold will be disabled temporarily, which is done to avoid removing objects that could be linked to the upcoming new revision. This parameter can be changed from the [configuration](pantavisor-configuration.md#pantavisorconfig).

### On Demand

Finally, our garbage collector can be triggered on demand issuing a [command](pantavisor-commands.md#commands) through Pantavisor control socket.

## Integrity

Pantavisor offers **secureboot**, a security mechanism to ensure integrity of the artifacts that are part of a [revision](revisions.md). It can perform a double level validation: artifact checksum and state signature.

### Artifact Checksum

Objects and JSONs belonging to the revision that is about to be run after booting up are checked against the checksums stored in their state JSON by default.

This verification can be [disabled](pantavisor-configuration.md#pantavisorconfig) if boot up speed needs to be boosted.

### State Signature

Additionally to artifact checksum, artifacts that form a revision can also be signed. This can be done from your host computer or from an automated CI, and it is then validated from Pantavisor using a cryptographic hash algorithm. It can be [tuned](pantavisor-configuration.md#pantavisorconfig) according to one of these three levels of severity:

* disabled: no signature validation will be performed.
* audit: all validations from lenient and strict will be performed, errors will be reflected in logs but nothing will fail.
* lenient: only the signed artifacts in the revision will be validated. Enabled by default.
* strict: all artifacts in the revision must be signed and will be validated.

Right now, RS256, ES256, ES384 and ES512 algorithms are supported and Pantavisor must know the public key either by storing it on disk directly or by using a certificate chain.

Validation is performed in Pantavisor in two places:

* when booting up, right after Pantavisor is initialized and before the current [revision](revisions.md) is loaded. In case of failure, the update will be reported as [ERROR](updates.md) if possible and the device will either rollback or just reset.
* when a new [remote update](remote-control.md#pantacor-hub-client) is received or a new [local update](local-control.md#local-control) is [run](pantavisor-commands.md#commands). In case of failure, the update will be reported as [WONTGO](updates.md).

In either case, Pantavisor will parse all [signatures](pantavisor-state-format-v2.md#_sigscontainerjson), parse the [protected JSON](pantavisor-state-format-v2.md#protected), calculate the hash depending on the elements included in the JSON and compare them with the signatures.

#### On-disk public key

In this option, Pantavisor will need the RSA public key to be stored on disk. To do so, it is necessary to add it during [compiling time](build-options.md#copy-pem-file-in-the-initrd-rootfs).

It will need the signature and information about how to generate it in the [revision checkout](pantavisor-state-format-v2.md#_sigscontainerjson).

#### Certificate chain of trust

In this case, the public key will travel in the [revision checkout](pantavisor-state-format-v2.md#_sigscontainerjson) in a x509 certificate. The rest of the chain can be added to the checkout, but the root certificate have to be stored on disk during [compiling time](build-options.md#copy-certificate-file-in-the-initrd-rootfs).

It will also need the signature and information about how to generate it in the [revision checkout](pantavisor-state-format-v2.md#_sigsjson).
