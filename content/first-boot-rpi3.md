---
Title: Booting a newly Pantavisor-enabled Raspberry Pi
Description: Learn how to insert a flashed SD card, connect the Internet via wifi or ethernet cable and boot up the Pantavisor-enabled device. 
---

# Booting a Pantavisor-enabled Raspberry Pi

Insert the flashed SD card into the board. Then connect the device to an internet-facing network. This can be Ethernet or WiFi. Both methods are described in the steps below.

## Insert SD card into the board

![](images/insert-sd-card.png)

## Supply power to your board

Do that by connecting a USB micro cable from your computer to your board:

![](images/supply-power.png)

If everything went well, the PWR led (red) should now turn on, while the ACT led (green) will continue blinking. That means the board is up and running.

## Setting up the Internet connection to your board

### Setting up a WiFi portal for your Raspberry Pi

If a network cable is not an option, you can use WiFi. To connect your Raspberry Pi to the WiFi network, provide the SSID and password to it. This can be accomplished with a smartphone or any other device with wireless capabilities.

The first step is to wait a couple of minutes after boot up until you find a connection with SSID "Wifi Connect" and no password in your computer or smartphone:

![](images/wifi-connect-list-cropped.png)

After you connect, a captive portal pops up on your phone:

![](images/wifi-connect-captive-cropped.png)

Enter the portal, select the WiFi network and enter the password.

!!! Note 
   Your password will only be stored locally on your device.

![](images/wifi-connect-select-cropped.png)

The Raspberry Pi now has your network credentials saved locally and will automatically connect to that SSID after every reset.

![](images/wifi-connect-reset-cropped.png)

If you get this page, the device takes up to 2 minutes to synchronize with Pantacor Hub. You can continue to the next section [Discovering your device](discover-rpi3.md).

### Connecting an ethernet cable

#### Ethernet network connection

The easiest way to connect your device to the Internet is with a cable that is directly connected to your Internet-enabled network. Plug one end of the ethernet cable to your router or switch and the other into your Raspberry Pi.

You can continue to the next section [Discovering your device](discover-rpi3.md).

#### Ethernet on PC

Alternatively you can use you an already connected computer to set up a shared ethernet connection for your Raspberry Pi.

Use an ethernet cable to connect your board to your computer.

After the board is up and LAN connection is on, the orange LED on the ethernet cable should start blinking.

![](images/lan-connection.png)

Now, follow your OS specific instructions to activate network sharing:

#### Ethernet on Linux

**NOTE:** These instructions are for Ubuntu only.

1. Click on the network icon and go to ```Edit Connections...```
2. Double click on your wired connection:
   * Switch to the ```IPv4 Settings``` tab
   * Select ```Shared to other computers``` method
   * Click on ```Save...```

#### Ethernet on Mac OS

1. Open System Preferences
2. Select Sharing Preference Panel
3. Enable Internet Sharing with the following settings:
   * Share your connection from: Wi-Fi
   * To computers using: Ethernet

![](images/enable-internet-sharing.png)

#### Ethernet on Windows

1. Go to Network Connections
2. Open your WiFi connection's properties:
   * Switch to the ```Sharing``` tab
   * Enable it
   * Select Ethernet as the ```Home networking connection```
   * Click on ```Apply```
