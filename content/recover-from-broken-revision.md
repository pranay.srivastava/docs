# Recover Broken System Revision

Pantavisor will automatically revert to the previous revision if a newly pushed version does not allow the device to connect to the Internet.

Besides that, you can redeploy an old revision from the Pantahub interface. Just go to your pantahub.com dashboard and look for your device. In the History tab of your device you will find a list of revisions with its timestamp, status and commit message. At the right column, you have two buttons: one let's you redeploy that revision, the other one is for navigating to that revision in Pantahub.

![](images/deploy-this-revision.png)

The number of the current running revision will be automatically sent by the device right after it gets network connectivity. You can check this and other useful data in the Metadata tab of your device in Pantahub.
