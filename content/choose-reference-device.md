# Choose your Reference Device

**IMPORTANT:** This guide focuses on building Pantavisor from source code. If you do not need to compile Pantavisor, pre-compiled Pantavisor images are available for all supported targets in our [download](initial-devices.md) page.

To produce an image you will need the Pantavisor source code that you retrieved above as well as a Pantavisor reference device. The final build image will contain the BSP generated from source code plus the containers from that device. This Pantavisor reference device can be either stored in Pantahub or locally in your machine. By default, if no reference device is specified, the build system will generate an image that has no containers (therefore no functionality).

For instance, if you want to produce an image that has the same app stack than one of your online devices, you would use the pvr ```clone URL``` of that device as a parameter to build pantavisor. If you do not have your own custom device developed yet, you can use any of our initial devices as your starting point:

```
https://pvr.pantahub.com/pantahub-ci/malta_initial_latest
```

You can get this and other reference devices in our [download page](initial-devices.md), in the ```Pantahub devices``` column of the table.

Bear in mind that you will have to choose one that matches the architecture of your device. Current supported targets are ```[arm-generic, arm-bpi-r2, malta-qemu, mips-generic, arm-rpi0w, arm-rpi3, arm-rpi4, x64-uefi, x64-uefi-installer]``` and a few other test ones. Note down the ```Target``` value of your initial device. In the case of the previous example, it would be:

```
malta-qemu
```

If you rather want to use one of the devices you have developed and burn an image from it, you would go to your device details page on https://www.pantahub.com and get the pvr ```Clone URL``` from there:

![](images/pantahub-device-details-1.png)
