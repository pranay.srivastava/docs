# Discover your device

Your device is synchronizing right now with our Pantacor Hub platform.
You can discover your device in your Pantacor Hub web interface, by using the `pvr device scan` command or through pantabox with the [`pantabox-claim`](https://docs.pantahub.com/pvr-sdk/reference/pantabox-claim/)command.

If you are unable to find your device in the following steps, please refer to the [Troubleshooting](board-troubleshooting-rpi3.md) section.

## Web Interface Pantacor hub

Your device should now show up on the right side of your [Pantacor Hub Dashboard](https://hub.pantacor.com/).
The initial device name is randomized.
![](images/ph-landing-page.png)
You can open the device's dashboard by clicking on it.
There, you can rename the device and see all sorts of device information.

![](images/ph-device-dashboard.png)

## Command PVR

To discover your device from the command line interface, use the command `pvr device scan`.
This command discovers all Pantahub devices in your network and prints non-sensitive device information.

![](images/pvr-discover-rpi.png)

## Claiming the Device with Pantabox

To claim a device through Pantabox, you must have already downloaded an initial precompiled image, flashed your device and logged in to the device. Go to one of the Getting Started Guides, either [Raspberry Pi](get-started.md) or [Other Devices](choose-image.md) and then follow the instructions on the screen to log in.  After you've logged in to Pantabox, claim the device with [`pantabox-claim`](https://docs.pantahub.com/pvr-sdk/reference/pantabox-claim/)command.