# First Run

Now that you have [installed](install-appengine.md) Pantavisor App Engine, let us see how to run it:

```
sudo /opt/pantavisor/pantavisor -i
```

This will do the job! But maybe you prefer to add that to the init system of your Linux distro. In that case, remove the interactive (-i) option. Take a look at other options with:

```
/opt/pantavisor/pantavisor -h
```

If your device is connected to the Internet, you will be able to [claim](claim-device.md#claim-your-device-manually-from-the-target) this device and take control from [Pantacor Hub](remote-control.md#pantacor-hub) using the credentials that you will find in these directories:

```
/opt/pantavisor/var/device-id
/opt/pantavisor/var/challenge
```

One thing to consider is that this device will start with no containers running. To do so and start getting the most out of Pantavisor, you can follow our documentation on how-to [manage your device](install-pvr.md) from your host computer to install containers and much more.
