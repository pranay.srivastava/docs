# Pantavisor State Format

Pantavisor state format describes the json format to configure the Pantavisor run-time state. In this page, we are going to go through the System State Format v2, which is the version we currently use.

**NOTE:** This reference is for advanced users. Before manually editing these values in your device, check if what you intend to do can be achieved with the help of [pvr](pvr).

## Spec Format

A complete system is made up of one BSP as well as one to many containers. The state allows this following this format:

```
{
    "#spec": "pantavisor-service-system@1",
  
    "README.md": "xxx",

    "bsp/run.json": {..},
    "bsp/file1....": {..},

    "<container1>/run.json": {..},
    "<container1>/file1...": "xxx",

	"disks.json": {..},

	"_sigs/<container1>.json": {..},

	"_config/<container1>/file1...": "xxx"
}
```

This table defines all possible keys for the state format:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| #spec | _pantavisor-service-system@1_ | **mandatory** | version of the state format |
| README.md | string | empty | readme file in markdown format |
| bsp/run.json | [bsp/run.json](#bsprunjson) | **mandatory** | bsp run-time configuration file |
| bsp/drivers.json | [bsp/drivers.json](#bspdriversjson) | empty | managed drivers |
| <container\>/run.json | [container/run.json](#containerrunjson) | **mandatory** | container run-time configuration file |
| disks.json | [disks.json](#disksjson) | empty | definition of physical store medium |
| \_sigs/container.json | [\_sigs/container.json](#_sigscontainerjson) | empty | signature file |
| \_config/<container\>/<path\> | [\_config/container/](#_configcontainer) | empty | configuration files |

### bsp/run.json

This file allows to configure the [BSP](bsp.md). The JSON follows this format:

```
{
	"addons": [
		"addon-plymouth.cpio.xz4"
	],
	"firmware": "firmware.squashfs",
	"initrd": "pantavisor",
	"initrd_config": "trail.config",
	"linux": "kernel.img",
	"modules": "modules.squashfs"
}
```

This table defines all possible keys for the BSP run.json:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| addons | list of paths | **mandatory** | list of relative paths to the [initrd rootfs addons](bsp.md#addons) |
| firmware | path |  **mandatory** | relative path to Linux firmware file |
| initrd | path |  **mandatory** | relative path to Pantavisor binary file |
| initrd_config | path | emtpy | relative path to [revision level config](pantavisor-configuration.md#after-an-update) |
| linux | path |  **mandatory** | relative path to Linux kernel file |
| modules | path |  **mandatory** | relative path to Linux modules file |

### bsp/drivers.json

This JSON file can be used to set a list of [managed drivers](bsp.md#managed-drivers), following this format:

```
{
    "#spec": "driver-aliases@1",
    "all": {
        "bluetooth": [
            "hci_uart",
            "btintel"
        ],
        "wifi": [
            "iwlwifi ${user-meta:drivers.iwlwifi.opts}"
        ]
    },
    "dtb:all": {}
}
```

This table defines all supported keys for the BSP drivers.json:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| #spec | _driver-aliases@1_ | **mandatory** | version of the drivers JSON |
| all | list of [managed drivers](#managed-driver) | **mandatory** | list of managed driver JSONs |

#### managed driver

Each driver JSON is composed by a list of Kernel modules. It follows this format:

```
"bluetooth": [
    "hci_uart",
    "btintel ${user-meta:drivers.btintel.opts}"
]
```

This table defines all supported keys for the BSP driver JSON:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| name | string | **mandatory** | name of the driver |
| modules | list of strings | empty | module names with optional user or device metadata key where the load arguments are stored |

### container/run.json

This file allows to individually configure each [container](containers.md). This JSON follows this format:

```
{
	"#spec": "service-manifest-run@1",
	"config": "lxc.container.conf",
	"name": "awconnect",
	"root-volume": "root.squashfs",
	"storage": {
		"docker--etc-NetworkManager-system-connections": {
			"persistence": "permanent"
		},
		"lxc-overlay": {
			"persistence": "boot"
		}
	},
	"type": "lxc",
	"volumes": [],
	"drivers": {
		"required": [ "ethernet" ]
	}
}
```

This table defines all possible keys for the container run.json:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| #spec | _service-manifest-run@1_ | **mandatory** | version of the run JSON |
| config | path | **mandatory** | relative path to the container configuration file |
| name | string | **mandatory** | name of the container |
| root-volume | path | **mandatory** | relative path to the container rootfs file |
| storage | [storage](#storage) | **mandatory** | container storage configuration |
| type | _lxc_ | **mandatory** | type of container |
| volumes | list of paths | **mandatory** | relative paths to the additional container volumes |
| logs | list of [loggers](#logger) | empty | list of log configuration JSONS |
| group | string | empty | name of [group](containers.md#groups) |
| runlevel | _data_, _root_, _platform_ or _app_ | _root_ or _platform | _DEPRECATED: use group instead |
| roles | _mgmt_ | empty | list of roles for the container |
| status_goal | _MOUNTED_, _STARTED_ or _READY_ | _MOUNTED_ or _STARTED_ | [status goal](containers.md#status-goal) |
| restart_policy | _system_ or _container_ | [system or container](containers.md#groups) | [transition type](updates.md#inprogress) when container is updated |
| drivers | list of [container drivers](#container-driver) | empty | list of referenced [managed drivers](#managed-driver) for the container |

#### storage

This JSON allows to configure the [storage of a container](containers.md#storage). It is important to notice that you can configure the overlay or any of the container rootfs paths:

```
{
	"lxc-overlay":{
        "persistence":"boot"
    },
	"etc/example/": {
        "persistence": "permanent"
    },
	"docker--etc-modules-load.d": {
		"disk": "built-in",
		"persistence": "permanent"
	},
	"docker--var-pv": {
		"disk": "dm-versatile",
		"persistence": "permanent"
	},
	...
}
```

This table defines all possible keys for the container storage JSON:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| persistence | _permanent_, _revision_ or _boot_ | **mandatory** | 
| disk | string | empty | name of disk defined in [disks.json] |

#### logger

This allows to configure a [logger inside of a container](containers.md#logger). It looks like this:

```
{
	"file":"/var/log/syslog",
	"maxsize":102485760,
	"truncate":true,
	"name":"alpine-logger"
}
```

This table defines all possible keys for the container logger JSON:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| _file_, _lxc_ or _console_ | string | **mandatory** | path of a file in the rootfs in case of _file_ key. _enable_ in case of _lxc_ or _console_ key |
| maxsize | integer | **mandatory** | max file size in bytes before truncating |
| truncate | _true_ or _false_ | **mandatory** | truncate the file when reaching maxsize |
| name | string | **mandatory** | logger daemon name |

### container driver

This JSON allows to configure a [container driver](containers.md#drivers). An example would look like:

```
"optional": [ "usbnet", "wifi" ]
```

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| loading | _required_, _optional_ or _manual_ | **mandatory** | how and when to load the driver |
| drivers | list of [managed driver names](#managed-driver) | empty | the managed drivers to be loaded |

### disks.json

This JSON allows to add new [disks](storage.md#disks). An example of this JSON shows it is just a list of JSONs with the following format:

```
[
    {
        "default": "yes",
        "name": "built-in",
        "path": "/storage/disks/",
        "type": "directory"
    },
    {
        "name": "dm-versatile",
        "options": "....",
        "path": "/storage/dm-crypt-file/disk1/versatile.img,8,versatile_key",
        "type": "dm-crypt-versatile"
    },
    {
        "name": "dm-caam",
        "options": "....",
        "path": "/storage/dm-crypt-file/disk1/caam.img,8,caam_key",
        "type": "dm-crypt-caam"
    },
    {
        "name": "dm-dcp",
        "options": "....",
        "path": "/storage/dm-crypt-file/disk1/dcp.img,8,dcp_key",
        "type": "dm-crypt-dcp"
    }
]
```

This table defines the keys for each disk:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| default | _yes_ or _no_ | _no_ | all volumes without specific [disk name](#storage) will use this disk |
| name | string | **mandatory** | unique name of the disk to be used in [container storage](#storage) |
| path | path | **mandatory** | destination mount path in case of directory. For crypt disk, please refer below |
| type | _directory_, _dm-crypt-versatile_, _dm-crypt-caam_ or _dm-crypt-dcp_ | **mandatory** | type of disk |
| options | string | empty | options to be passed for mount command -o |

Device mapper crypt disk follows a special pattern in path:

```
image,size,key
```

image: disk path which will be encrypted (created during first boot if not present)
size: size in MB used to create the disk file
key: name of the key file which needs to be created using respective tools (e.g. caam-keygen for i.Mx CAAM)

For example:

```
"path": "/storage/dm-crypt-file/disk1/file.img,8,key"
```

### \_sigs/container.json

This JSON allows to [sign](storage.md#integrity) a group of artifacts from the state. It must follow the standard [JWS format](https://datatracker.ietf.org/doc/html/rfc7515):

```
{
	"#spec": "pvs@2",
	"protected": "XXXX",
	"signature": "XXXX"
}
```

This table defines the keys for the signature JSON:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| #spec | _pvs@2_ | **mandatory** | version of the signature JSON |
| protected | base64 encoded [protected](#protected) | **mandatory** | information about how the signature was created |
| signature | base64 encoded hash | **mandatory** | the signature itself |

#### protected

This JSON allows to reproduce the signature hash from [\_sigs/container.json](#sigscontainerjson). It must follow the standard [JWS format](https://datatracker.ietf.org/doc/html/rfc7515):

```
{
	"alg":"RS256",
	"typ":"PVS",
	"pvs": {
		"include":["pv-avahi/**","_config/pv-avahi/**"],
		"exclude":["pv-avahi/src.json"]
	},
	"x5c": [
		"XXXX",
		"XXXX"
	]
}
```

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| alg | _RS256_, _ES256_, _ES384_ or _ES512_ | **mandatory** | cryptography algorithm to generate the signature |
| typ | _PVS_ | **mandatory** | type of signature |
| pvs | lists of _included_ and _excluded_ paths | **mandatory** | relative paths to the files that were used to generate the signature |
| x5c | list of certificate strings | empty | certificate that contains the public key plus more certificates from the chain of trust |

### \_config/container/

This directory can be used to attach [additional files](containers.md#additional-files) to the container rootfs.

For example, if we wanted to overwrite the main pvr-sdk configuration file, we would do it by adding the desired file to the \_config directory in the JSON state:

```
_config/pvr-sdk/etc/pvr-sdk/config.json
```

Which would set that file in the pvr-sdk container rootfs like this:

```
/etc/pvr-sdk/config.json
```

## Format Exceptions

The files in this section are meant to be used for tooling (like [pvr](install-pvr.md)) and thus ignored by Pantavisor.

### bsp/src.json

```
{
  "#spec": "bsp-manifest-src@1",
  "pvr": "https://pvr.pantahub.com/pantahub-ci/arm_rpi64_bsp_latest#bsp"
}
```

### bsp/build.json

```
{
  "altrepogroups": "",
  "branch": "master",
  "commit": "e2a4911eb35de2032e85f74c8f239de81c6f622b",
  "gitdescribe": "014-rc14-18-ge2a4911",
  "pipeline": "436189414",
  "platform": "rpi64",
  "project": "pantacor/pv-manifest",
  "pvrversion": "pvr version 026-52-gbf3bd5d6",
  "target": "arm-rpi64",
  "time": "2021-12-24 01:25:27 +0000"
}
```

### container/src.json

```
{
  "#spec": "service-manifest-src@1",
  "docker_config": {
    "AttachStderr": false,
    "AttachStdin": false,
    "AttachStdout": false,
    "Cmd": [
      "/sbin/init"
    ],
    "Domainname": "",
    "Env": [
      "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    ],
    "Hostname": "",
    "Image": "sha256:701f4bbc1d5b707fc7daabf637cb2fa12532d05ceea6be24f8bb15a55805843b",
    "OpenStdin": false,
    "StdinOnce": false,
    "Tty": false,
    "User": "",
    "WorkingDir": ""
  },
  "docker_digest": "registry.gitlab.com/pantacor/pv-platforms/pv-avahi@sha256:895b2af2b5d407235f2b8c7c568532108a44946898694282340ef0315e2afb28",
  "docker_name": "registry.gitlab.com/pantacor/pv-platforms/pv-avahi",
  "docker_source": "remote,local",
  "docker_tag": "arm32v6",
  "persistence": {},
  "template": "builtin-lxc-docker"
}
```
