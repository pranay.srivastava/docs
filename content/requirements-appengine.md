# Pantavisor App Engine

Pantavisor App Engine [init mode](init-mode.md) lets you run Pantavisor as a daemon in your already set up Linux distro. This is a less intrusive way to try Pantavisor without having to flash your device storage. Pantavisor App Engine will not get you to the [minimal](specs.md) system we strive for. However, it will be a perfect tool for quick and non-disruptive prototyping of our [container engine](pantavisor-architecture) in your devices.

## Requirements

The installation script of App Engine needs `patchelf` to be installed in your system:

```
sudo apt update
sudo apt install patchelf
```
