---
title: Get Started with Pantavisor on Raspberry Pi
description: Learn how to get started with Pantavisor and Pantacor Hub on Raspberry Pi with a pre-compiled flashable image.  
---
# Getting started on a Raspberry Pi
This hands-on tutorial is the fastest way to get started with Pantavisor and Pantacor Hub. You'll flash your device with a ready-to-use Raspberry Pi pre-compiled image.

## Requirements
Before starting this guide, you may want to create a Pantacor Hub account and install the `pvr` command line tools as stated in the [Working with Pantavisor](before-you-begin.md).  

!!! Note
    If you don't want to sign up for Pantacor Hub and use the PVR cli, you can use Pantabox to work with Pantavisor after you've downloaded the pre-compiled initial image. Pantabox is included on the pre-compiled image. 

To follow this guide, you need the following:

- Raspberry Pi 3 B+ or Raspberry Pi 4 
- a micro SD Card
- micro SD Card Writer

If you are missing any of those requirements, you can have a look at [How to install other images](choose-image.md), such as an emulated Malta with QEMU, or you can also [build your own image](environment-setup.md). But keep in mind that if you go this route, you will have to use the PVR CLI and Pantacor Hub. 


## Tutorial overview
1. [Image setup](image-setup-rpi3.md): Download and flash our pre-compiled Pantavisor image on a micro SD card.
2. [First boot](first-boot-rpi3.md): Insert the micro SD card in the board slot and boot it up.
3. [Discover your device](discover-rpi3.md): Discover your running Pantavisor device in the Pantacor Hub interfaces.

After this, you will be able to remotely manage your newly claimed device using Pantacor Hub. See the [how-to guide on maintaining](clone-your-system.md) your device for more information on how to develop and deploy software.
