# Clone Your Systems

If you prefer working from your host computer, the first step is to ```clone``` any of the revisions on the device before making any changes to the running device.

This can be done either from the cloud or from the device itself.

## Clone from Pantacor Hub cloud

Pantavisor managed systems automatically sync their config and runtime factory state on first contact with their Pantacor Hub instance immediately after [claiming](claim-device.md) your device.

After the sync, the system runtime state is retrieved through a Pantacor Hub endpoint. To make this easy, the ```pvr``` cli provides the ability to ```clone``` the entire system.

To clone a system, copy the ```pvr``` Clone URL that appears in the Pantacor Hub device dashboard and use pvr:

```
pvr clone https://pvr.pantahub.com/user1/device1 my-checkout
```

```pvr``` asks you to log in, then it downloads the objects and unpacks them locally on disk.

## Clone from a local device

You can also clone the currently running revision of a device that is connected to your local network.

First, you will need to know your device IP. If your host computer is in the same network, you can [scan](https://docs.pantahub.com/pvr/#pvr-device-scan) your device.

After that, clone the revision into your computer with:

```
pvr clone http://192.168.1.122:12368/cgi-bin/pvr my-checkout
```
