# Pantavisor Architecture

At a high level, Pantavisor is just in charge of two things: container orchestration and communication with the outside world.

## Container Orchestration

The software that is running in a Pantavisor-enabled device at a certain moment is called a [revision](revisions.md). A revision is composed by a [BSP](bsp.md) and a number of [containers](containers.md). Pantavisor is able to go from the current running revision to a new revision in a [transactional manner](updates.md).

Revisions, as well as other relevant data, are [stored](storage.md) on-disk in the device to make them persistent.

## Communication with the Outside World

Pantavisor-enabled devices need to communicate with the outside world to consume new [updates](updates.md), exchange [metadata](metadata.md) or send [logs](storage.md#logs). Users can achieve this [remotely](remote-control.md) as well as [locally](local-control.md).
