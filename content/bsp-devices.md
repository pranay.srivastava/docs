# Pantavisor BSP

## Update a Pantavisor BSP

These BSPs are stored on Pantahub devices, and can be used from your device checkout with pvr:

```
pvr merge https://pvr.pantahub.com/pantahub-ci/arm_rpi3_bsp_latest#bsp
pvr checkout
```

### Stable

**IMPORTANT:** this devices contain both the stable and the release candidate versions. Navigate through the device _History_ tab to switch to the version you want to use.

| Platform | Target | Pantahub devices |
 | --- | --- | --- |
 | **arm-generic** | arm-generic | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f6d0eaa73f000a7159ca)
**bpi-r2** | arm-bpi-r2 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f6ddeaa73f000a715a24)
**malta** | malta-qemu | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f83d82f42a000ac1af5c)
**mips-generic** | mips-generic | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f70182f42a000ac1a927)
**rpi0w** | arm-rpi0w | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f715a223e2000a2d3236)
**rpi3** | arm-rpi3 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5eba6457eaa73f000a2dd24f)
**rpi4** | arm-rpi4 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f72deaa73f000a715bc5)
**x64-generic** | x64-generic | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ecbd1e518f6c6000a904c69)
**x64-uefi** | x64-uefi | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f73c82f42a000ac1aa33) |

### Latest

| Platform | Target | Pantahub devices |
 | --- | --- | --- |
 | **arm-generic** | arm-generic | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f6c818f6c6000a6200f1)
**bpi-r2** | arm-bpi-r2 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f6d8a223e2000a2d30fb)
**malta** | malta-qemu | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f83782f42a000ac1af44)
**mips-generic** | mips-generic | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f70982f42a000ac1a937)
**rpi0w** | arm-rpi0w | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f71082f42a000ac1a951)
**rpi3** | arm-rpi3 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5eba6446eaa73f000a2dd16b)
**rpi4** | arm-rpi4 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f727eaa73f000a715bb5)
**x64-generic** | x64-generic | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ecbd1e0a223e2000a5b699d)
**x64-uefi** | x64-uefi | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ec4f735a223e2000a2d327e) |

## About Pantavisor BSP

The BSP CI builds daily and stable versions of our BSP for several targets so you can use them and focus on what's really important: your Pantavisor app development.

The BSP devices are automatically upgraded with GitLab CI in this [project](https://gitlab.com/pantacor/pv-manifest). Check out the jobs in the [pipeline list](https://gitlab.com/pantacor/pv-manifest/pipelines) and the release notes for each stable version in the [list of tags](https://gitlab.com/pantacor/pv-manifest/-/tags).


