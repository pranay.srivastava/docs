# BSP

In addition to [containers](containers.md), Pantavisor is in charge of the life-cycle of the Linux kernel, modules and firmware. To allow upgrading all this plus Pantavisor itself, all these binaries have been included under the BSP denomination in the [state JSON](pantavisor-state-format-v2.md#bsprunjson). 

## Pantavisor

The Pantavisor binary and dependency tree are part of the revision BSP in the state JSON.

### Addons

Additional files can be [added](pantavisor-state-format-v2.md#bsprunjson) to Pantavisor initrd rootfs without having to do it during compile time.

It is important to remark that the binary file has to be under cpio.xz4 compression. That is, cpio.xz with 4 Byte alignment. You can take a look at how we do the cpio.xz4 compression at [this example](https://gitlab.com/pantacor/pantavisor-addons/gdbserver). For the rest of gdbserver installation and use, go [here](debug-pantavisor.md).

## Linux Kernel

The Linux Kernel, modules and firmware are also part of the revision BSP of the state JSON.

### Managed Drivers

Pantavisor offers a [declarative way](pantavisor-state-format-v2.md#bspdriversjson) to define a list of drivers at BSP level, each driver being just a set of one to many Kernel modules. Parameters for Kernel loading are supported too by mapping drivers to [device or user metadata](https://docs.pantahub.com/metadata.md).

[Containers](containers.md#drivers) can then make use of these BSP managed drivers by referencing them.

## Bootloader

To natively run Pantavisor on a device, it is necessary to have some on-disk artifacts that fall out of the umbrella of Pantavisor [revisions](revisions.md). This is the case of the bootloader, which will load the Linux kernel and directly execute the initrd (Pantavisor) after that. Both the bootloader an Pantavisor will interact with [Pantavisor storage](storage.md) to communicate with each other.

We support a number of boards, using both u-boot and grub bootloaders. These include all the bring up stuff that is necessary to run Pantavisor in its minimal form. You can take a look at the supported boards [here](initial-devices.md). Contact us if you need support for your board!
