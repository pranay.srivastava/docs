# Pantavisor initial devices

## Flash a Pantavisor initial image

If you need to **install a base image from scratch** for your project, continue to our [initial images page](initial-images.md).

## Update a Pantavisor initial device

To use these devices, you will need an already flashed and claimed device in your account. If you don't have one, go to the [initial images section](#flash-a-pantavisor-initial-image).

If what you need is to only update the Pantavisor BSP of your device, go to the [bsp devices page](bsp-devices.md). If you want to update the BSP plus the initial development apps, you can upgrade your device checkout with pvr. Just remember to change the pvr clone URL with the one corresponding to your platform:

```
pvr merge https://pvr.pantahub.com/pantahub-ci/rpi3_initial_latest
pvr checkout
```

### Stable

**IMPORTANT:** this devices contain both the stable and the release candidate versions. Navigate through the device _History_ tab to switch to the version you want to use.

| Platform | Target | Pantahub devices |
 | --- | --- | --- |
 | **bpi-r2** | arm-bpi-r2 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5df2423794a0930009512edb)
**colibri-imx6dl** | arm-toradex-colibri-imx6dl | [device](https://www.pantahub.com/u/pantahub-ci/devices/5f4f7222fffe62000a44842b) |
**colibri-imx6ull** | arm-toradex-colibri-imx6ull | [device](https://www.pantahub.com/u/pantahub-ci/devices/5f3a5a3eefa001000a85cf58) |
**malta** | malta-qemu | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d553ba8e8c4940008979089)
**rpi0w** | arm-rpi0w | [device](https://www.pantahub.com/u/pantahub-ci/devices/5e5d06854795a60009a2827e)
**rpi3** | arm-rpi3 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5e43ca659411e3000a647871)
**rpi4** | arm-rpi4 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5df242c8b02db000091a28f4)
**x64-uefi** | x64-uefi | [device](https://www.pantahub.com/u/pantahub-ci/devices/5df242b508ef8d000946adf2) |

### Latest

| Platform | Target | Pantahub devices |
 | --- | --- | --- |
 | **bpi-r2** | arm-bpi-r2 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d64df359061a500090ea599)
**colibri-imx6dl** | arm-toradex-colibri-imx6dl | [device](https://www.pantahub.com/u/pantahub-ci/devices/5f4f7244fffe62000a448560) |
**colibri-imx6ull** | arm-toradex-colibri-imx6ull | [device](https://www.pantahub.com/u/pantahub-ci/devices/5f3a59fd84781e000abfd0d2) |
**malta** | malta-qemu | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d553b5ae8c4940008978a8e)
**rpi0w** | arm-rpi0w | [device](https://www.pantahub.com/u/pantahub-ci/devices/5e5cea9c745f14000a4221ea)
**rpi3** | arm-rpi3 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d35db3c4db15e0008fef281)
**rpi4** | arm-rpi4 | [device](https://www.pantahub.com/u/pantahub-ci/devices/5ddbf18787f1d100082dce13)
**x64-uefi** | x64-uefi | [device](https://www.pantahub.com/u/pantahub-ci/devices/5d838a31f99f9c00095b2f21) |

## About Pantavisor initial devices

Our initial devices CI keeps a set of devices of different architectures up to date with both stable and daily BSP builds plus some apps that will help you start developing your own Pantavisor apps. It also generates flashable images for the stable versions.

These base images are built using GitLab CI in this [project](https://gitlab.com/pantacor/ci/pv-initial-devices). Check out the jobs in the [pipeline list](https://gitlab.com/pantacor/ci/pv-initial-devices/pipelines) and the release notes for each stable version in the [list of tags](https://gitlab.com/pantacor/ci/pv-initial-devices/-/tags).
