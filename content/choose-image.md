# Choose your Pre-Compiled Image

You can get Pantavisor images for all supported targets in our [download page](initial-images.md). That image can be used as a base system for your projects, as well as just a demo to try Pantavisor out, without having to build from source code.

In the download page, you will be able to navigate through latest (daily built), stable and the rest of our builds. Each build produces the images for every supported target. Furthermore, the Pantahub reference device for that build is included, so you will be able to inspect the contents of the initial image, that allows knowing which BSP and containers does the initial version have. For more info on how the images are built, check our [build how-to](environment-setup.md).

Just choose a version, a target architecture, and click on the download link. You will get a xz compressed file, which you will have to decompress to get your flashable image:

```
unxz rpi3.img.xz
```
