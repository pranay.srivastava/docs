# Get the source code

This build system is based on the Alchemy tool, plus modifications specific to the requirements of Pantavisor-enabled images. Make a new workspace directory:

```
mkdir pantavisor
cd pantavisor
```

To initialize it, use [`repo`](https://source.android.com/source/using-repo). You can download the necessary code using the -g option. Just substitute ```malta-qemu``` with the target you [want to build](choose-reference-device.md).

```
repo init -u https://gitlab.com/pantacor/pv-manifest -m release.xml -g runtime,malta-qemu
```

You may prefer to download the source code for all targets. In that case:

```
repo init -u https://gitlab.com/pantacor/pv-manifest -m release.xml
```

Now, it is time to download the code with the sync command:

```
repo sync -j10 -c --no-clone-bundle
```

Afterwards, you should have contents similar to below in our Pantavisor directory:

```
alchemy  bootloader  build.docker.sh  build.sh  config
external  internal  kernel  out  scripts  toolchains  vendor
```
