# Build Options

Image options at compile time.

## Set a reference device

When building Pantavisor, it is mandatory to set a reference device with the ```PVR_MERGE_SRC``` environment variable. All the apps from the reference device will be used for the new image.

The value can be either a pvr clone URL, or a local path to a [cloned](clone-your-system.md) device checkout. The first option would look like this:

```
PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable \
    ./build.docker.sh arm-rpi3
```

While for building with a cloned device reference, we have to indicate the path to the .pvr folder inside the checkout:

```
PVR_MERGE_SRC=device-checkout/.pvr \
    ./build.docker.sh arm-rpi3
```

## Build debug system images

If you are developing pantavisor or some low level plumbing yourself you might need special facilities such as console output on [TTY, SSH, etc.](inspect-device.md) that we disable in normal builds by default.

To enable debugging facilities you can pass the ```PANTAVISOR_DEBUG``` environment variable to the build. The Default is ```no```:

```
PANTAVISOR_DEBUG=yes \
    PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable \
    ./build.docker.sh arm-rpi3
```

**IMPORTANT:** Bear in mind that if you are using the board [TTY](inspect-device.md#tty) shell that comes with this option, Pantavisor will not be able to reboot or poweroff the board until you manually ```exit``` the shell. This will not happen while conected to the board via [SSH](inspect-device.md#ssh).

## Debug build system

This can be useful to get all commands executed during the build process. We are going to use ```MAKEFLAGS```, which is an environment variable to set additional options to the building of each one of the target modules.

The flags we are going to set are ```V=s``` to set the verbosity level, and ```-j1``` to disable parallel compilation and get an easier to read build log:

```
MAKEFLAGS="V=s -j1" \
    PVR_MERGE_SRC=device-checkout/.pvr \
    ./build.docker.sh arm-rpi3
```

## Use auto-claim feature

The public pantavisor distribution supports to seed a shared secret (called 'autotok') during build that will make a device automatically join a user account of the operator.

Before you can build an image that has this token "built-in" you will have to acquire a token from pantahub. You can read how to do that in our [Pantahub API Reference](https://docs.pantahub.com/pantahub-base/devices/#auto-assign-devices-to-owners).

Not how you can not only acquire a token, but also specify default user-meta fields to seed on devices that join using that token. This will allow you to tag devices by product-id etc. for your management tooling etc.

Following that guideline you will be able to acquire a token. You now have to pass that token to the Pantavisor build system to produce a prebuilt image that will automatically join your account. To do so you use the ```PV_FACTORY_AUTOTOK``` environment variable when building the image:

```
PV_FACTORY_AUTOTOK=xxxxYOURTOKENxxxx \
    PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable \
    PANTAVISOR_DEBUG=yes \
    ./build.docker.sh arm-rpi3
```

## Non interactive build

This can be interesting for automated pantavisor build. By default build.docker.sh will ask for credentials when building against a pantahub URL. This can be avoided with the ```ACCESS_TOKEN``` and ```PV_BUILD_INTERACIVE``` environment variables.

First, you need to get a Pantahub token for your pvr credentials. You can read how to do this in our [Pantahub API Reference](https://docs.pantahub.com/pantahub-base/auth/#authenticate).

Now, you can use the previously mentioned environment variables along your newly generated token:

```
ACCESS_TOKEN=$TOKEN \
    PV_BUILD_INTERACIVE=false \
    PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable \
    ./build.docker.sh arm-rpi3
```


## Image Only Build

If you want to avoid building the bsp and use the one from the device specified in ```PVR_MERGE_SRC```, you can use the ```PVR_USE_SRC_BSP``` option. Thiswill skip the bsp building part and just assembly the image with the bsp and apps from ```PVR_MERGE_SRC```. It will save most of the building time.

For example, to build a Raspberry 4 image that uses our initial images PVR repo to retrieve apps and bsp bits you can use:

```
PVR_USE_SRC_BSP=yes \
	PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi4_initial_stable \
	./build.docker.sh arm-rpi4
```

This would build just the necessary host tools as well as uboot (which is currently not shipped inside the trail bsp/).

## Copy PEM file in the initrd rootfs

One PEM file can be added to Pantavisor rootfs so it can be used for the [secureboot](storage.md#integrity) feature. The key is ```PV_PVS_PUB_PEM``` and it must point to the path of the file in the host computer.

For example:

```
PV_PVS_PUB_PEM=/home/anibal/pantacor/src/devices/pub.pem \
	PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi64_initial_stable \
	./build.docker.sh arm-rpi64
```

## Copy certificate file in the initrd rootfs

A root CA can be added to Pantavisor rootfs so it can be used for the [secureboot](storage.md#integrity) feature. The key is ```PV_PVS_CERT_PEM``` and it must point to the path of the file in the host computer.

For example:

```
PV_PVS_CERT_PEM=/home/anibal/pantacor/src/devices/ca.pem \
	PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi64_initial_stable \
	./build.docker.sh arm-rpi64
```
