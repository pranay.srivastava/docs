# Get the Logs

There are two ways of getting the logs from your device: web or console.

The logs can be monitored from the [Pantahub Dashboard](https://hub.pantacor.com/), in the ```Console``` tab:

![](images/ph-logs.png)

If you prefer to use the terminal, you can use the ```pvr logs``` command to conveniently tail the current log stream of your device. For example, if we wanted to get the pantavisor logs from device 5df394ec94a09300095bfab7 and revision 291, we would use:

```
pvr device logs --rev 291 --source=/pantavisor.log 5df394ec94a09300095bfab7
```

If you want to stop the upload of logs to the cloud from your device, you can use [metadata](pantavisor-metadata.md#user-metadata) during runtime, or using [pantavisor configuration](pantavisor-configuration.md#pantahub.config). If you prefer to change how these logs are pushed or add another logs from your app, continue to our [push logs page](push-logs-to-pantahub.md).

## Notes: how does the device send the logs to Pantahub?

Getting the logs for your systems happen through the pantahub /logs endpoint. By default, Pantavisor will itself use such log facilities to make all relevant logging information accessible.
