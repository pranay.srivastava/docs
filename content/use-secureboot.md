# Secure your Revision

This page shows how to make Pantavisor check the integrity of your revision with the [secureboot feature](storage.md#integrity). We are going to do this with two simple examples.

First, it is necessary to bear in mind that [secureboot](pantavisor-configuration.md#pantavisor.config) is set to _lenient_ by default in the configuration. This means that only the signed artifacts will be verified by Pantavisor but if we wanted it to check that all artifacts in the revision were signed, we would need to set [secureboot](pantavisor-configuration.md#pantavisor.config) to _strict_.

Now, the path that we are going to follow is going to depend on how do we want the public key to be used by Pantavisor.

## Use an on-disk public key

To generate a new key pair, we can use openssl:

```
mkdir keys; cd keys
openssl genrsa -out priv.pem 2048
openssl rsa -in priv.pem -outform PEM -pubout -out pub.pem
cd ..
```

With the keys already created, we have to put the public key (pub.pm) in the initrd rootfs. To so so, you will need to [build Pantavisor](environment-setup.md) with [this option](build-options.md#copy-pem-file-in-the-initrd-rootfs):

```
PV_PVS_PUB_PEM=/home/anibal/keys/pub.pem \
PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi64_initial_stable \
./build.docker.sh arm-rpi64
```

Now let us sign one of the platforms (in this case, pv-avahi) with [pvr](install-pvr.md):

```
cd my-checkout
pvr sig --key ../keys/priv.pem add -p pv-avahi
pvr add .
pvr commit
pvr post
```

And we are set! Pantavisor will now verify the signature of the pv-avahi artifacts using the on-disk public key.

## Use a certificate chain of trust

To generate a chain of certificates:

```
mkdir keys; cd keys
openssl genrsa  -out myCA.key 2048
openssl req -x509 -new -nodes -key myCA.key -sha256 -days 1825 -out myCA.pem
openssl genrsa  -out myKey.key 2048
openssl req -nodes -new -key myKey.key -out myKey.csr
openssl x509 -req -in myKey.csr -CA myCA.pem -CAkey myCA.key -CAcreateserial -out myKey.crt -days 825 -sha256 
cd ..
```

We need to put the root certificate (myCA.pem) in the initrd rootfs. For that, you will need to [build Pantavisor](environment-setup.md) with [this option](build-options.md#copy-certificate-file-in-the-initrd rootfs).

```
PV_PVS_PUB_PEM=/home/anibal/keys/myCA.pem \
PVR_MERGE_SRC=https://pvr.pantahub.com/pantahub-ci/rpi64_initial_stable \
./build.docker.sh arm-rpi64
```

Finally, to sign one of the platforms (pvr-sdk, for example) with [pvr](install-pvr.md):

```
cd my-checkout
pvr sig --x5c ../cert/myKey.crt --key ../cert/myKey.key add --part pvr-sdk
pvr add .
pvr commit
pvr post
```

Pantavisor will now verify the signature of the pvr-sdk artifacts using the key in the checkout certificate. It will also check the chain of trust is good using the on-disk root certificate.
