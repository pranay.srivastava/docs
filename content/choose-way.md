# Choose your Way

We try to allow management of your boards in different ways and not force a single path. With management, we mean updating the device, getting its logs, sending and receiving other useful metadata, etc. This can be done in two ways: locally or through the cloud. The [local experience](local-control.md#local-control) will let you do these things directly from your host computer into your device, while the cloud will let you perform the management of devices using [Pantacor Hub](remote-control.md#pantacor-hub) or [any other cloud](remote-control.md#other-remote-controllers) of your choice.

If this is your first time though, we recommend you to go with [Pantacor Hub](register-user.md). It is simple and visual, but you can also go the local experience way and use [pantabox](inspect-device.md) to get a menu UI from your device.

Sooner or later, you will need the power of CLI in order to manage devices from your host, and we offer a tool named [pvr](install-pvr.md) for that. It supports the latest features, including both local and cloud experiences.

!!! Note
    These tools can be used simultaneously and one path will not close others, so you can try it all and keep the best one (or ones) for you!
