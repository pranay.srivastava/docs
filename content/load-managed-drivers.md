# Load Managed Drivers

Pantavisor supports the management of drivers by letting you define a [list of drivers at BSP level](bsp.md#managed-drivers) and then reference them from [containers](containers.md#drivers).

These drivers can be then loaded at different points of execution depending on how we configure them.

In this guide, we are going to explain how we would go about loading a WiFi driver that can be loaded at any point of execution from the container.

## State JSON

Firstly, we are going to define the list of managed drivers at the BSP level in the [pvr checkout](clone-your-system.md). For that, we are going to create a new [bsp/drivers.json](pantavisor-state-format-v2.md#bspdriversjson):

```
{
    "#spec": "driver-aliases@1",
    "all": {
        "wifi": [
            "iwlwifi ${user-meta:drivers.iwlwifi.opts}"
        ]
    }
}
```

We have set a new driver named _wifi_, that is composed solely by one Linux module, _iwlwifi_. Additionally, we have added the ability to set arguments for `modprobe` using the [user metadata](metadata.md#user-metadata) key _drivers.iwlwifi.opts_.

Then, we need to tell Pantavisor that the _wifi_ driver will be available from the containers. We do so by adding this information to [container/run.json](pantavisor-state-format-v2.md#containerrunjson). In this case, we are setting the drivers on _manual_ mode, as we want to load it at any point from the container, but we could set _required_ or _optional_ of we wanted it to be loaded as soon as the container started:

```
{
	...
	"drivers": {
		"manual": [ "wifi" ],
	}
}
```

You can avoid to manually edit container/run.json, as pvr offers templating support to generate the right run.json.

For that, you have the template "args" keys PV_DRIVERS_REQUIRED, PV_DRIVERS_OPTIONAL and PV_DRIVERS_MANUAL available in src.json.

See the example below:

```
{
	...
	"args": {
		"PV_DRIVERS_MANUAL": ["wifi"]
	}
}
```

## modprobe Arguments

To define additional arguments to `modprobe`, we have previously set a user metadata key named _drivers.iwlwifi.opts_.

It is important to notice that [device metadata](metadata.md#device-metadata) can also be used for this purpose, but we have defined user metadata in the [state JSON](#state-json). User metadata can be set using the [control socket](local-control.md) in local mode. In remote mode, you would do that through [Pantacor Hub](remote-control.md#pantacor-hub).

From the container and in local mode, you can set user metadata with the following curl command.

```
$ curl -X PUT --unix-socket /pantavisor/pv-ctrl \
	--data="power_save=true power_level=5"  \
	http://localhost/user-meta/drivers.iwlwifi.opts
```

If you prefer, you can also do this with [pvcontrol](local-control.md#pvcontrol).

This will practically result in a module being loaded using the following modprobe command next time the "wifi" driver is loaded:

```
$ modprobe iwlwifi power_save=true power_level=5
```

## Load Driver

To load the _wifi_ driver from the container, simply use:

```
$ curl -X PUT --unix-socket /pantavisor/pv-ctrl http://localhost/drivers/wifi/load
```
