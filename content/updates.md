# Updates

Getting back to [revisions](revisions.md), these can be modified and upgraded, thus creating a new revision of the device.

Updates will be processed differently depending on the source being [remote](remote-control.md#pantacor-hub) or [local](local-control.md).

Pantavisor will [report](pantavisor-architecture.md#communication-with-the-outside-world) the update state from the moment the update is acknowledged until it is finished. Pantavisor will only progress to the new revision in case of success. Otherwise, no action or rollback will ensue, depending on the point where the error is detected. This is the list of update states:

* [QUEUED](#queued)
* [DOWNLOADING](#downloading)
* [INPROGRESS](#inprogress)
* [TESTING](#testing)
* [UPDATED](#updated)
* [DONE](#done)
* [WONTGO](#wontgo)
* [ERROR](#error)

## QUEUED

Only valid for [remote](remote-control.md#pantacor-hub) updates.

Pantavisor has got the [state JSON](pantavisor-state-format-v2.md) of the new [revision](revisions.md), but is performing other operations and has put it to the queue to be processed later.

## DOWNLOADING 

Only valid for [remote](remote-control.md#pantacor-hub) updates.

Downloading the artifacts for the new revision.

## INPROGRESS

Installing or progressing to this revision. Transitions to new revisions can either require a [reboot](#reboot-transition) or [not](#non-reboot-transition).

To finish this state, it is necessary that all [status goals](containers.md#status-goal) existing in the new revision have been achieved. Also, in the case of a [remote](remote-control.md#pantacor-hub) update, Pantavisor needs to have performed communication with Pantacor Hub. If these two conditions are not met within a [configurable](pantavisor-configuration.md#pantavisor.config) time, Pantavisor will [rollback](#error) the revision.

### Reboot transition

Reboot transitions are performed based on the location of the changes belonging to the new [revision](revisions.md) update:

* In the root of the [status JSON](pantavisor-state-format-v2.md#spec-format)
* In the [BSP](bsp.md)
* In any of the containers with a _system_ [restart policy](containers.md#restart-policy)
* In any [additional file](containers.md#additional-files) that belongs to a container with _system_ [restart policy](containers.md#restart-policy)
* In any [additional file](containers.md#additional-files) that does not belong to any container

In this case, Pantavisor will stop all the containers and reboot the board.

### Non-reboot transition 

Non-reboot transitions are performed after an update that does not contain any changes in any of the components described for the [reboot updates](#reboot-updates).

In this case, Pantavisor will only stop the containers that were affected by the update and restart them with the recently installed new revision artifacts.

## TESTING

Waiting to see if the revision is stable. During this stage, Pantavisor checks if all containers are running and will [rollback](#error) if any of them exits. Besides that, in the case of a [remote](remote-control.md#pantacor-hub) update, it will also [rollback](#error) in case Pantacor Hub communication is lost.

## UPDATED

The revision is stable, but the update did not need a board reboot, so the rollback point is not set until you [force a reboot](pantavisor-commands.md#commands).

## DONE

The revision has been fully booted and is stable. The rollback point is set.

## WONTGO

The new revision cannot be parsed because of a bad [state JSON](pantavisor-state-format-v2.md) format, so it is aborted.

## ERROR

The new revision failed during INPROGREES or TESTING stages. Pantavisor will try to rollback to the latest [DONE](#done) revision.
