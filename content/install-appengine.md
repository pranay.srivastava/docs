# Installation

First step will be to download our pre-compiled tarball to your device. You can do that using one of the `appengine` targets that matches your device's architecture from our [download page](initial-images.md):

```
mkdir /tmp/appengine
wget https://pantavisor-ci.s3.amazonaws.com/pv-initial-devices/tags/018-rc12/622479249/x64_appengine_initial_stable.appengine.tar.xz -O /tmp/appengine/appengine.tar.xz
```

Next step is to decompress and run the installation script setting the installation path of your choice. This will take care of the rest of the setup:

```
tar -xf /tmp/appengine/appengine.tar.xz -C /tmp/appengine/
sudo /tmp/appengine/install /opt/pantavisor
```

That should take charge of installing App Engine in your system, if you want to see other options for the installation script, you can do it by:

```
/tmp/appengine/install -h
```

To uninstall Pantavisor App Engine, just run the script that will be present in the installation path:

```
sudo /opt/pantavisor/uninstall
```

This will clean any trace of Pantavisor, its dependencies, installed revisions and Pantacor Hub credentials (Pantacor Hub will not be able to communicate with your device anymore).
