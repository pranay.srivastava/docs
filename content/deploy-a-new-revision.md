# Deploy a new System Revision

Once you have done some changes like described in previous section, you can deploy the currently staged pvr state to any device for which you have owner permissions.

For that use ```pvr post``` command.

```pvr post``` has an optional argument where you can specify a pvr clone URL for the device you want the currently staged system state to be posted to.

Once post has been submitted to a device, the device will eventually wake up and try to consume the new state.

## Notes: How does the update process work?

First, disk space will be checked for the new revision. If space is not enough, Pantavisor will try to make some room for the update with our [garbage collector](storage.md#garbage-collector), which will remove artifacts and logs from older revisions.

After that, the process follows with the proper [installation](updates.md) and [transition](updates.md#inprogress) to the new revision.
