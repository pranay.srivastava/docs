# Pantavisor Configuration

This reference is for advanced users. Before manually editing these values in your config, check if what you intend to do can be achieved through one of the [build options](build-options.md).

There are four ways to tweak Pantavisor configuration, depending on when it can be modified: at compile time, at boot up time, after an update or during runtime. Bear in mind that each level in this list will overwrite whatever is configured in the previous ones.

## At compile time

You can take a look at the default configuration files we use to build our supported platforms [here](https://gitlab.com/pantacor?utf8=%E2%9C%93&filter=pv-config).

### pantavisor.config

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| cache.usrmetadir | path | /storage/cache/meta | set persistent user metadata dir |
| cache.devmetadir | path | /storage/cache/devmeta | set persistent device metadata dir |
| system.init.mode | embedded, standalone or appengine | embedded | pantavisor init mode |
| system.libdir | path | /lib | set lib path |
| system.etcdir | path | /etc | set etc path |
| system.rundir | path | /pv | set run path |
| system.mediadir | path | /media | set media path |
| system.confdir | path | /configs | set config path |
| debug.shell | 0 or 1 | 1 | enable debug shell |
| debug.ssh | 0 or 1 | 1 | enable debug ssh server |
| dropbear.cache.dir | path | /storage/cache/dropbear | set dropbear cache path |
| meta.cache.dir | path | N/A | _DEPRECATED: moved to cache.usrmetadir in [pantavisor.config](#pantavisorconfig)_ |
| bootloader.type | uboot, uboot-pvk or grub | uboot | set type of bootloader |
| bootloader.mtd_only | 0 or 1 | 0 | enable MTD |
| bootloader.mtd_env | path | N/A | set MTD path |
| secureboot.mode | disabled, audit, lenient or strict | lenient | set [secureboot](storage.md#integrity) severity level |
| secureboot.certdir | path | /certs | set cert path |
| secureboot.checksum | 0 or 1 | 1 | enable objects and jsons checksum validation |
| storage.device | path | N/A | set storage path |
| storage.fstype | ext4, ubifs or jffs2 | N/A (mandatory) | journaling file system |
| storage.opts | N/A | N/A | not implemented |
| storage.mntpoint | path | N/A (mandatory) | set pv mount point |
| storage.mnttype | ext4 | disabled | pv mount file system |
| storage.logtempsize | size | disabled | sets the size of memory dedicated for log storage. The size may have a k, m or g suffix or a % suffix for percentage of memory |
| storage.wait | integer | 5 | wait for storage to be available |
| storage.gc.reserved | integer | 5 | [garbage collector](storage.md#garbage-collector) will try to always keep this percentage of disk free |
| storage.gc.keep_factory | 0 or 1 | 0 | avoid deletion of revision 0 objects by the [garbage collector](storage.md#garbage-collector) |
| storage.gc.threshold | integer | 0 | [garbage collector](storage.md#garbage-collector) will be triggered if there is less than this percentage of disk free. 0 means it will just be run when an update needs more space than available |
| storage.gc.threshold.defertime | integer | 600 | time in seconds the [garbage collector threshold](storage.md#threshold) will be deferred after a new object has been put from the [pvctrl objects endpoint](pantavisor-commands.md#objects) |
| disk.voldir | path | /volumes | set volumes path |
| updater.goals.timeout | integer | 120 | set time in seconds before an update is considered to have failed if not all [status goals](containers.md#status-goal) have been met at that point |
| updater.use_tmp_objects | 0 or 1 | 0 | download objects in an on-disk temporary location. If disabled, objects will be stored in memory while downloading |
| revision.retries | integer | 10 | max number of update retries before rollback |
| revision.retries.timeout | integer | 120 | set time in seconds before a failed update is retired |
| wdt.enabled | 0 or 1 | 1 | enable kernel watchdog |
| wdt.timeout | integer | 15 | set reset timeout in seconds for kernel watchdog |
| net.brdev | string | lxcbr0 | container bridge name |
| net.braddress4 | ip | 10.0.3.1 | container bridge address |
| net.brmask4 | ip | 255.255.255.0 | container bridge mask address |
| log.dir | path | /storage/logs/ | set log storage path |
| log.server.outputs | comma-sepated list of filetree or singlefile | filetree | defines output of [log server](storage.md#logs) |
| log.maxsize | integer | 2097152 B (2 MB) | max size of a log file before rotating |
| log.level | 0 (FATAL), 1 (ERROR), 2 (WARN), 3 (INFO), 4 (DEBUG) or 5 (ALL) | 5 | pantavisor log will only capture messages from the set loglevel to 0 |
| log.buf_nitems | integer | 128 KB | log buffer size |
| log.capture | 0 or 1 | 1 | store logs. Disable will stop log push too |
| log.loggers | 0 or 1 | 1 | disable loggers that tail traditional log files used inside containers - often seen in system containers |
| log.stdout | 0 or 1 | 0 | enable pantavisor logs to go to stdout aka dmesg; remember to set ignore_loglevel and printk.devkmsg=on in /proc/cmdline to get all messages on console |
| libthttp.log.level | 0 (FATAL), 1 (ERROR), 2 (WARN), 3 (INFO), 4 (DEBUG) or 5 (ALL) | 3 | libthttp log will obly capture messages from set level to 0 |
| lxc.log.level | integer | 2 | 0 to 8 lxc log verbosity level |
| control.remote | 0 or 1 | 1 | enable all communication with Pantahub |

Example of pantavisor.config:

```
bootloader.type=normal-uboot
storage.device=LABEL=pvroot
storage.fstype=ext4
storage.opts=
storage.mntpoint=/storage
wdt.enabled=1
wdt.timeout=30
log.level=5
log.buf_nitems=128
```

### pantahub.config

| Key | Key (cmdline) | Value | Default | Description |
|-----|-------|-------|---------|-------------|
| creds.type | builtin and ext-* | builtin | set type of authentication |
| creds.host | ip | 192.168.53.1 | Pantahub IP |
| creds.port | port | 12365 | Pantahub port |
| creds.proxy.host | proxy host | NULL | Proxy Host (if enabled) |
| creds.proxy.port | proxy port | 3128 | Proxy Port |
| creds.proxy.noproxyconnect | int | 0 | Disable Proxyconnect protocol (basic http proxy for TLS) |
| creds.id | string | unset (set after claim) | device id |
| creds.prn | string | unset (set after claim) | device prn |
| creds.secret | string | unset (set after claim) | device secret |
| creds.tpm.key | string | disabled | TMP key |
| creds.tpm.cert | string | disabled | TMP certificate |
| factory.autotok | string | disabled | token for auto claiming |
| updater.keep_factory | 0 or 1 | N/A | _DEPRECATED: moved to storage.gc.keep_factory in [pantavisor.config](#pantavisorconfig)_ |
| updater.interval | integer | 60 | time between Pantahub requests during waiting operation |
| updater.network_timeout | integer | 120 | wait time in seconds before rollback if device does not get network connectivity |
| updater.commit.delay | integer | 180 | testing time in seconds after an update |
| log.dir | path | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| log.maxsize | integer | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| log.level | 0 (FATAL), 1 (ERROR), 2 (WARN), 3 (INFO), 4 (DEBUG) or 5 (ALL) | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| log.buf_nitems | integer | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| log.push | 0 or 1 | 1 | push logs to cloud. Can be overriden using [metadata](pantavisor-metadata.md#user-metadata) |
| log.capture | 0 or 1 | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| log.loggers | 0 or 1 | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| log.stdout | 0 or 1 | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| libthttp.log.level | 0 (FATAL), 1 (ERROR), 2 (WARN), 3 (INFO), 4 (DEBUG) or 5 (ALL) | N/A | _DEPRECATED: moved to [pantavisor.config](#pantavisorconfig)_ |
| metadata.devmeta.interval | integer | 10 | time between Pantahub devmeta requests |

Example of pantahub.config:

```
updater.interval=5
updater.network_timeout=60
updater.commit.delay=40
creds.host=api.pantahub.com
creds.port=443
creds.id=
creds.prn=
creds.secret=
```

## At boot up time

The values from the previous sections can be overriden at boot up time, just writing the key=value pair in `/proc/cmdline` with a prefix before the key. This is tipically done from the [bootloader](bsp.md#bootloader).

Any key from [pantavisor.config](#pantavisorconfig) can be overriden appending the _pv\__ prefix to the key, while any [pantahub.config](#pantahubconfig) can do the same with the _ph\__ prefix.

## After an update

Some of the values from the previous sections can be overridden when making a new [revision](revisions.md). To do so, you must create a new file, following the same format we use for [compile time configuration](#at-compile-time) and reference it from [bsp/run.json](pantavisor-state-format-v2.md#bsprunjson).

These are the keys that are accepted at this level of configuration. See the [compile time section](#at-compile-time) for reference:

* storage.wait
* storage.gc.reserved
* storage.gc.keep_factory
* storage.gc.threshold
* storage.gc.threshold.defertime
* updater.goals.timeout
* updater.use_tmp_objects
* revision.retries
* revision.retries.timeout
* updater.keep_factory
* updater.interval
* updater.network_timeout
* updater.commit.delay
* creds.proxy.host
* creds.proxy.port
* creds.proxy.noproxyconnect
* log.maxsize
* log.level
* log.buf_nitems
* log.push
* log.capture
* libthttp.log.level
* lxc.log.level
* metadata.devmeta.interval

For example, this is how this file would look like if we wanted to overwrite the log level and the garbage collector threshold:

```
log.level=3
storage.gc.threshold=30
```

## During runtime

[User metadata](metadata.md#user-metadata) can be used to override the values from the previous sections.

These are the keys that are accepted in metadata configuration. See the [compile time section](#at-compile-time) for reference:

* storage.gc.reserved
* storage.gc.keep_factory
* storage.gc.threshold
* storage.gc.threshold.defertime
* updater.interval
* log.level
* log.push
* libthttp.log.level
* metadata.devmeta.interval
