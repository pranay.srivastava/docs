# Inspect your Device

This section explains how to connect to your Pantavisor device from your host computer in an easy way.

**IMPORTANT:**: The image will have to be built with the [PANTAVISOR_DEBUG](build-options.md#build-debug-system-images) option.

## SSH

Before we start, we will have to create a ssh public key and set it up in our device metadata. In Linux, you can create a ssh pair and print your public key with these commands:

```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
cat ~/.ssh/id_rsa.pub
```

Now, create a new [metadata pair](set-device-metadata.md) for the [SSH public key](pantavisor-metadata.md#user-metadata):

```
key: pvr-sdk.authorized_keys
value: <ssh pub id>
```

![](images/user-meta.png)

**IMPORTANT**: It is assumed for all commands that ```10.0.0.1``` is the reachable IP address, however this might be different depending on what you get from your local network. You can check the assigned IPs for all network intefaces in your pantahub.com device's metadata. It is asumed also that the container to reach is alpine-hotspot, but this can be changed to any app running on the device.

SSH server (Dropbear) should be accessible at port 8222 and the desired container name as user:

```
ssh -p 8222 alpine-hotspot@10.0.0.1
```

You can directly reach Pantavisor with the "/" user (root):

```
ssh -p 8222 /@10.0.0.1
```

## TTY

An alternative to all the previous network accessed options, is to get into Pantavisor with a TTY shell. This can be achieved either with a cable (in case you are using a [RPi3 or RPi4](board-troubleshooting-rpi3.md#tty)) or from the QEMU console (if you are emulating the image with [QEMU](#malta) from a your host).

After booting up, you will have two chances to stop the boot up and get a shell that will allow you to interact with the boot up process. First one will be before the bootloader loads the kernel and Pantavisor. If you press any key during when prompted, you will get to the bootloader shell:

```
U-Boot 2016.09-g15600e2 (Jan 15 2021 - 16:18:19 +0000)

Board: MIPS Malta CoreLV
DRAM:  128 MiB
Flash: 16 MiB
In:    serial@3f8
Out:   serial@3f8
Err:   serial@3f8
Net:   pcnet#0
Warning: pcnet#0 MAC addresses don't match:
Address in SROM is         02:b8:2a:7f:48:b6
Address in environment is  52:54:00:12:34:56
, pcnet#1
Hit any key to stop autoboot:  0 
malta #
```

After kernel and Pantavisor is loaded, you can also access Pantavisor user space from an ash shell. For that, just press ```d``` when prompted:

```
[    1.685794] Run /init as init process
Press [d] for debug ash shell... 5 d

#
```

Pantavisor will not reboot while the debug shell is open. This will allow to debug faulty revisions that might need a reset or a rollback. To enable reboot and power off again, use ```exit``` command to close the shell.

## pventer

Once we are inside Pantavisor's root container (`/`) via SSH or TTY, we can access the different app using the ```pventer``` command. To get a session in the alpine-nginx app:

```
pventer -c alpine-nginx
```
