---
Title: Finding the target image for your device
Description: Images are pre-compiled according to the architecture of the target device.
---
# Preparing the image for your target device

Pantavisor images are built specifically for the architecture of a target device.

## Target Device Architectures

### RPi3, RPi4, BPi-R2 or RPi0

Pantavisor images for the Raspberry Pi boards are: arm-rpi3, arm-rpi4, arm-bpi-r2 and arm-rpi0w.

To use these images for the above targets, you need to flash your SD card and then boot the board up. You can see how to do this in the [getting started guide](image-setup-rpi3.md). This guide focuses on RPi3, but the process is similar for all of these devices.

### Malta

The Pantavisor target image for malta is: malta-qemu.

We prepared a Docker container that allows you to run the malta image. Keep in mind though that the script that runs, changes the network configuration on your host:

```
docker run --privileged --net host -v </your/host/path/to/pflash.img>:/tmp/pflash.img -it --rm pantacor/qemu-malta-16m
```

When the initialization script and boot up log ends, press ENTER to see the LEDE console.

### x64-uefi

There are two types of Pantavisor images for these boards: x64-uefi and x64-uefi-installer.

These images boot from an external storage device (SD card, USB key..) from BIOS/UEFI. The non-installer image is meant to be directly flashed onto the final storage device where the system runs. The installer image will then be flashed into a temporary storage device. To see how to flash your storage device view this [getting started guide](image-setup-rpi3.md). Even though this guide is for RPi3, the flashing process is similar.

Once this is done, insert the storage device into your x64 device and power it up. Access the BIOS/UEFI menu and boot the target device from the storage device, if that is not the default boot option. Notice that you will need to do this with a keyboard and screen connected to your device.

#### non-installer

If you are using the non-installer image, Pantavisor boots from the storage device.

#### installer

For the installer, you need a keyboard and a screen connected to your target device.

After booting up, the installer prompts with a menu and automatically flashes the first available permanent storage device found. A reboot is required after this finishes.

For troubleshooting or to simply choose the permanent storage device from a list of flashable devices, select the manual installation option in the menu before it triggers the automatic installation. Instructions will follow on screen that describe how to do this from the console.

## Troubleshooting

Our pre-compiled images are built with the [PANTAVISOR_DEBUG](build-options.md#build-debug-system-images) option. This means you will have [SSH, TTY and other goodies](inspect-device.md) available for debugging your devices. Most of the items explained in our getting started [troubleshooting](board-troubleshooting-rpi3.md) section are applicable here.
