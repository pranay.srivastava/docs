# Add Additional Files to Apps

We have seen how apps can be [added and updated](pvr-docker-apps-experience.md) to your [pvr checkout](clone-your-system.md). Now, we are going to add or modify files inside of the app rootfs without having to generate a new one and update it. To do so, you can take advantage of our [additional files](containers.md#additional-files) feature.

Let us see how to do it for our previous `nginx` example. So far, our pvr checkout looks like this:

```
$ ls
awconnect  bsp  _hostconfig  network-mapping.json  pv-avahi  pvr-sdk  storage-mapping.json  webserver
```

Now, we are going to add a new [\_config](pantavisor-state-format-v2.md#_configcontainer) to the checkout with the name of the app and the path inside of its rootfs:

```
mkdir -p _config/webserver/etc
echo "hello world!" > _config/webserver/etc/test
```

This should result in this:

```
$ ls
awconnect  bsp  _config  _hostconfig  network-mapping.json  pv-avahi  pvr-sdk  storage-mapping.json  webserver
```

To push the changes:

```
pvr add .
pvr commit
pvr push
```

Now, if you [ssh](https://docs.pantahub.com/inspect-device) to the _webserver_ app, you will find the new file:

```
# cat /etc/test  
hello world!
```

This can be also useful to modify configuration files or any other content inside the app rootfs without having to produce a new one.
