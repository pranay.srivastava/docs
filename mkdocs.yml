site_name: Docs
theme:
  name: 'material'
  logo: /images/logo.svg
  favicon: images/favicon.png
  custom_dir: 'overrides'
  font: 
    text: Inter
    code: Roboto Mono    
extra:
  analytics:
    provider: google
    property: G-R935BJVEPH
docs_dir: content
extra_css:
  - stylesheets/extra.css
markdown_extensions:
  - toc:
      permalink: True
  - admonition
  - meta
plugins:
  - search
  - redirects:
        redirect_maps:
          'get-started-malta.md': 'get-source-code.md'
          'get-started-rpi3.md': 'get-started.md'
          'get-started-limesdr-rpi3.md': 'limesdr-initial-devices.md'
          'operation-modes.md': 'pantavisor-architecture.md'
          'container-management.md': 'pantavisor-architecture.md'
          'garbage-collector.md': 'storage.md'
          'pantahub-client.md': 'remote-control.md'
          'security.md': 'storage.md'
          'pantavisor-disks.md': 'storage.md'
          'bsp-configuration.md': 'pantavisor-state-format-v2.md'
          'app-configuration.md': 'pantavisor-state-format-v2.md'


nav:
  - 'Introduction':
    - 'What is Pantavisor?': index.md
    - 'Specs': specs.md
    - 'Licensing': licensing.md
  - 'Get Started':
    - 'Where to Start?': before-you-begin.md
    - 'Sign up to Pantacor Hub': register-user.md
  - 'How to Prepare your Device':
    - 'Choose a device': choose-device.md
    - 'Raspberry Pi':
      - 'Requirements': get-started.md
      - 'Image Setup': image-setup-rpi3.md
      - 'First Boot': first-boot-rpi3.md
      - 'Discover your device': discover-rpi3.md
      - 'Troubleshooting': board-troubleshooting-rpi3.md
    - 'Other Devices':
      - 'Choose your Image': choose-image.md
      - 'Image Setup': image-setup.md
    - 'App Engine':
      - 'Introduction': requirements-appengine.md
      - 'Installation': install-appengine.md
      - 'First Run': first-run-appengine.md
      - 'Update': update-pantavisor.md
  - 'How to Manage your Device':
    - 'Choose your Way': choose-way.md
    - 'From Pantacor Hub':
      - 'Claim your Device': claim-device.md
      - 'Get the Logs': get-the-logs.md
      - 'Recover Broken System Revision': recover-from-broken-revision.md
      - 'Set User Metadata': set-device-metadata.md
      - 'Push App Logs to Pantahub' : push-logs-to-pantahub.md
    - 'From the Host':
      - 'Installing PVR': install-pvr.md
      - 'Clone your System': clone-your-system.md
      - 'Make a new System Revision': make-a-new-revision.md
      - 'Deploy a new System Revision': deploy-a-new-revision.md
      - 'Add Apps from Docker': pvr-docker-apps-experience.md
      - 'Add Additional Files to Apps': pvr-config-files.md
      - 'Secure your Revision': use-secureboot.md
      - 'Load Managed Drivers': load-managed-drivers.md
    - 'From the Device':
      - 'Inspect your Device': inspect-device.md
  - 'How to Build':
    - 'Apps':
      - 'Create your Apps': create-apps.md
      - 'Communicate with Pantavisor': send-commands-pantavisor.md
    - 'Pantavisor BSP':
      - 'Environment Setup': environment-setup.md
      - 'Choose your Reference Device': choose-reference-device.md
      - 'Get the Source Code': get-source-code.md
      - 'Build Pantavisor': build-pantavisor.md
      - 'Debug Pantavisor': debug-pantavisor.md
      - 'Pantavisor Coding Style': pantavisor-coding-style.md
  - 'Technical Overview':
    - 'Pantavisor Architecture': pantavisor-architecture.md
    - 'Revisions': revisions.md
    - 'BSP': bsp.md
    - 'Containers': containers.md
    - 'Updates': updates.md
    - 'Storage': storage.md
    - 'Metadata': metadata.md
    - 'Remote Control': remote-control.md
    - 'Local Control': local-control.md
    - 'Init Mode': init-mode.md
  - 'Reference Pages':
    - 'Pantavisor':
      - 'Build Options': build-options.md
      - 'Build Individual Components': build-components.md
      - 'Pantavisor Configuration': pantavisor-configuration.md
      - 'Pantavisor State Format': pantavisor-state-format-v2.md
      - 'Pantavisor Metadata': pantavisor-metadata.md
      - 'Pantavisor Control Socket': pantavisor-commands.md
    - 'Pantavisor CI':
      - 'Pantavisor CI': device-ci/README.md
      - 'Update CI Reference': device-ci/doc/reference-update-ci.md
      - 'BSP CI Reference': device-ci/doc/reference-bsp-ci.md
      - 'PV Deps CI Reference': device-ci/doc/reference-pvdeps-ci.md
    - 'Pantabox':
      - 'pantabox': pvr-sdk/reference/pantabox.md
      - 'chpasswd': pvr-sdk/reference/pantabox-chpasswd.md
      - 'claim': pvr-sdk/reference/pantabox-claim.md
      - 'edit-config': pvr-sdk/reference/pantabox-edit-config.md
      - 'edit': pvr-sdk/reference/pantabox-edit.md
      - 'edit-sshkeys': pvr-sdk/reference/pantabox-edit-sshkeys.md
      - 'edit-usermeta': pvr-sdk/reference/pantabox-edit-usermeta.md
      - 'golocal': pvr-sdk/reference/pantabox-golocal.md
      - 'goremote': pvr-sdk/reference/pantabox-goremote.md
      - 'goto': pvr-sdk/reference/pantabox-goto.md
      - 'info': pvr-sdk/reference/pantabox-info.md
      - 'install': pvr-sdk/reference/pantabox-install.md
      - 'make-factory': pvr-sdk/reference/pantabox-make-factory.md
      - 'reboot': pvr-sdk/reference/pantabox-reboot.md
      - 'redeploy': pvr-sdk/reference/pantabox-redeploy.md
      - 'shutdown': pvr-sdk/reference/pantabox-shutdown.md
      - 'update': pvr-sdk/reference/pantabox-update.md
      - 'view': pvr-sdk/reference/pantabox-view.md
    - 'PVControl':
      - 'pvcontrol': pvr-sdk/reference/pvcontrol.md
    - 'PVR':
      - 'CLI': pvr/README.md
    - 'Pantacor Hub API':
      - 'apps': pantahub-base/apps/README.md
      - 'auth': pantahub-base/auth/README.md
      - 'devices': pantahub-base/devices/README.md
      - 'trails': pantahub-base/trails/README.md
      - 'objects': pantahub-base/objects/README.md
      - 'logs': pantahub-base/logs/README.md
      - 'subscriptions': pantahub-base/subscriptions/README.md
      - 'dash': pantahub-base/dash/README.md
      - 'healthz': pantahub-base/healthz/README.md
  - 'Other Tutorials & Showcases':
    - 'LimeSDR Mini USD on RPi': limesdr-initial-devices.md
    - 'Mozilla IoT Gateway as an App on Pantavisor Devices': mozilla-iot-gateway-as-a-pantavisor-app.md
    - 'Yocto Digital Signage on OpenWrt Router (ELC Talk 2019)': elc-2019-yocto-on-openwrt-on-bpi-r2.md
    - 'Installing App on Pantavisor Device for GPIO management via Pantahub': install-app-pantavisor-gpio-management.md
    - 'Installing simple Hello World App on Pantavisor Device': custom-hello-world-app.md
    - 'Enabling Azure Device Update on a Pantavisor device': adu-agent/README.md
  - 'Downloads':
    - 'Pantavisor initial devices': initial-devices.md
    - 'Pantavisor initial images': initial-images.md
    - 'Pantavisor BSP': bsp-devices.md
